using DS;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DS.Enumerations;
using DS.ScriptableObjects;

public class InteractuableObjectComponent : MonoBehaviour
{
    [Header("Interaction Data")]
    [SerializeField] private string interactionText;
    private bool canTalk = false;
    private bool canInteract = false;

    [Header("Components")]
    private DialogueController dialogueController;

    private void Awake()
    {
        dialogueController = GetComponent<DialogueController>();
    }

    private void Start()
    {
        if (dialogueController != null)
        {
            canTalk = true;
        }
    }

    private void Update()
    {
        if (canInteract && Input.GetKeyDown(KeyCode.E))
        {
            if (canTalk && !DialogueManager.i.IsTyping)
            {
                if (!DialogueManager.i.UI_Dialogue.gameObject.activeSelf)
                {
                    dialogueController.ShowDialogue(0);
                    DialogueManager.i.actualDialogueController = dialogueController;
                }
                else if (dialogueController.dialogue.DialogueType == DSDialogueType.SingleChoice)
                {
                    dialogueController.AdvanceDialogue(0);
                }
            }
            else if (canTalk && DialogueManager.i.IsTyping && DialogueManager.i.CanSkip)
            {
                DialogueManager.i.SkipTyping();
            }
            else if (!canTalk)
            {
                Debug.Log("Object Interaction");
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        ActivateUI(true);
        if (other.gameObject.tag == "Player")
        {
            canInteract = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        ActivateUI(false);
        if (other.gameObject.tag == "Player")
        {
            canInteract = false;
        }
    }

    private void ActivateUI(bool activate)
    {
        DialogueManager.i.ChangeInteractionText(interactionText);
        DialogueManager.i.UI_Interaction.gameObject.SetActive(activate);
    }
}
