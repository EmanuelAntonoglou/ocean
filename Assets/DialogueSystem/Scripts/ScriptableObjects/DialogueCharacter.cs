using UnityEngine;

[CreateAssetMenu(fileName = "New Dialogue Character", menuName = "ScriptableObjects/DialogueCharacter")]
public class DialogueCharacter : ScriptableObject
{
    public string CharacterName;
    public Sprite CharacterSprite;
}
