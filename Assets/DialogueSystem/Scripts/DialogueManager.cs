using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DS.Enumerations;
using DS.ScriptableObjects;
using TMPro;
using UnityEngine.UI;
using Unity.VisualScripting;

public class DialogueManager : MonoBehaviour
{
    [NonSerialized] public static DialogueManager i;

    [Header("Interaction Data")]
    [SerializeField] private float interactionCooldown = 0.25f;
    [NonSerialized] public bool InDialogue = false;
    [NonSerialized] public bool CanSkip = true;
    [NonSerialized] public DialogueController actualDialogueController;

    [Header("Dialogue References")]
    [NonSerialized] public Canvas UI_Dialogue;
    private TextMeshProUGUI TMP_Dialogue;
    private TextMeshProUGUI TMP_Name;
    private GameObject DialogueOptions;

    [Header("Interaction References")]
    public Canvas UI_Interaction;
    [NonSerialized] public TextMeshProUGUI TMP_Interaction;

    [Header("Typing Settings")]
    public float typingSpeed = 0.05f;
    [NonSerialized] public bool IsTyping = false;
    public List<AudioClip> typingSounds;
    private AudioSource audioSource;

    private void Awake()
    {
        CreateSingleton();
    }

    private void Start()
    {
        UI_Dialogue = transform.GetChild(0).GetComponent<Canvas>();
        TMP_Dialogue = UI_Dialogue.transform.Find("Panel Dialogue").Find("TMP_Dialogue").GetComponent<TextMeshProUGUI>();
        TMP_Name = UI_Dialogue.transform.Find("Panel Dialogue").Find("TMP_Name").GetComponent<TextMeshProUGUI>();
        DialogueOptions = UI_Dialogue.transform.Find("Options").gameObject;

        TMP_Interaction = UI_Interaction.transform.Find("TMP_Interaction").GetComponent<TextMeshProUGUI>();

        audioSource = GetComponent<AudioSource>();
    }

    public void CreateSingleton()
    {
        if (i == null)
        {
            i = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void ShowDialogueBox(bool show)
    {
        InDialogue = show;
        UI_Dialogue.gameObject.SetActive(show);
    }

    public void ShowOptions(DSDialogueSO dialogue, int optionsCount = 0)
    {
        int totalChildren = DialogueOptions.transform.childCount;

        for (int i = 0; i < totalChildren; i++)
        {
            GameObject child = DialogueOptions.transform.GetChild(i).gameObject;
            child.SetActive(false);
        }

        if (optionsCount > 1)
        {
            for (int i = 0; i < optionsCount && i < totalChildren; i++)
            {
                GameObject child = DialogueOptions.transform.GetChild(i).gameObject;
                child.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = dialogue.Choices[i].Text;
                child.SetActive(true);
            }
        }
    }

    public void ShowDialogueText(DSDialogueSO dialogue)
    {
        if (TMP_Dialogue != null)
        {
            TMP_Name.text = dialogue?.DialogueCharacter?.name;
            TMP_Dialogue.text = string.Empty;
            IsTyping = true;
            CanSkip = false;
            StartCoroutine(TypeText(dialogue?.Text));
            Invoke(nameof(EnableSkip), interactionCooldown);
        }
    }

    private void EnableSkip()
    {
        CanSkip = true;
    }

    public void SkipTyping()
    {
        if (TMP_Dialogue != null)
        {
            StopAllCoroutines();
            TMP_Dialogue.text = actualDialogueController.dialogue.Text;
            IsTyping = false;
            ShowOptions(actualDialogueController.dialogue, actualDialogueController.dialogue.Choices.Count);
        }
    }

    private IEnumerator TypeText(string text)
    {
        foreach (char c in text.ToCharArray())
        {
            TMP_Dialogue.text += c;

            if (typingSounds.Count > 0 && audioSource != null)
            {
                AudioClip randomClip = typingSounds[UnityEngine.Random.Range(0, typingSounds.Count)];
                audioSource.PlayOneShot(randomClip);
            }

            yield return new WaitForSeconds(typingSpeed);
        }

        IsTyping = false;
        CanSkip = true;
        ShowOptions(actualDialogueController.dialogue, actualDialogueController.dialogue.Choices.Count);
    }

    public void ChangeInteractionText(string interactionText)
    {
        TMP_Interaction.text = interactionText;
    }

    public void OnOptionPressed(int option) => actualDialogueController?.AdvanceDialogue(option);
}
