using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DS.Enumerations;
using DS.ScriptableObjects;

public class DialogueController : MonoBehaviour
{
    [Header("Dialogue Scriptable Objects")]
    [SerializeField] private DSDialogueContainerSO dialogueContainer;
    [SerializeField] private DSDialogueGroupSO dialogueGroup;
    [SerializeField] public DSDialogueSO dialogue;

    [Header("Dialogue Filters")]
    [SerializeField] private bool groupedDialogues;
    [SerializeField] private bool startingDialoguesOnly;

    [Header("Dialogue Indexes")]
    [SerializeField] private int selectedDialogueGroupIndex;
    [SerializeField] private int selectedDialogueIndex;


    public void AdvanceDialogue(int option = 0)
    {
        if (dialogue == null) return;

        if (dialogue.Choices[option].NextDialogue != null)
        {
            dialogue = dialogue.Choices[option].NextDialogue;
            ShowDialogue();
        }
        else
        {
            HandleEndOfDialogue();
        }
    }

    public void ShowDialogue(int option = 0)
    {
        DialogueManager.i.ShowDialogueBox(true);
        DialogueManager.i.ShowDialogueText(dialogue);
    }

    private void HandleEndOfDialogue()
    {
        DialogueManager.i.ShowDialogueBox(false);
        DialogueManager.i.ShowOptions(dialogue);
        SelectFirstDialogueInGroup();
    }

    public void ChangeDialogue(string dialogueName)
    {
        var dialoguesInGroup = dialogueContainer.DialogueGroups[dialogueGroup];
        var dialogueFound = dialoguesInGroup.FirstOrDefault(d => d.DialogueName == dialogueName);

        if (dialogueFound != null)
        {
            dialogue = dialogueFound;
            selectedDialogueIndex = dialoguesInGroup.IndexOf(dialogueFound);
        }
        else
        {
            Debug.LogWarning($"Dialogue '{dialogueName}' not found in the group '{dialogueGroup.GroupName}'.");
        }
    }

    public void SelectFirstDialogueInGroup()
    {
        if (!dialogueContainer.DialogueGroups.ContainsKey(dialogueGroup))
        {
            Debug.LogWarning($"Dialogue group '{dialogueGroup.GroupName}' not found in the dialogue container.");
            return;
        }

        var dialoguesInGroup = dialogueContainer.DialogueGroups[dialogueGroup];
        var firstDialogue = dialoguesInGroup.FirstOrDefault(d => d.IsStartingDialogue);

        if (firstDialogue != null)
        {
            dialogue = firstDialogue;
            selectedDialogueIndex = dialoguesInGroup.IndexOf(firstDialogue);
        }
        else
        {
            Debug.LogWarning($"No starting dialogue found in the group '{dialogueGroup.GroupName}'.");
        }
    }

    #region ChangeDialogueGroup
    public void ChangeDialogueGroup(string groupName)
    {
        var group = dialogueContainer.DialogueGroups.Keys.FirstOrDefault(g => g.GroupName == groupName);
        if (group != null)
        {
            dialogueGroup = group;
            SelectFirstDialogueInGroup();
        }
        else
        {
            Debug.LogWarning($"Dialogue group '{groupName}' not found in the dialogue container.");
        }
    }

    public void ChangeDialogueGroup(string groupName, string dialogueName)
    {
        var group = dialogueContainer.DialogueGroups.Keys.FirstOrDefault(g => g.GroupName == groupName);
        if (group != null)
        {
            dialogueGroup = group;
            ChangeDialogue(dialogueName);
        }
        else
        {
            Debug.LogWarning($"Dialogue group '{groupName}' not found in the dialogue container.");
        }
    }
    #endregion
}
