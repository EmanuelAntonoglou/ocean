using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueOptionsController : MonoBehaviour
{
    public void OnButtonPressed(int option)
    {
        DialogueManager.i.OnOptionPressed(option);
    }

}
