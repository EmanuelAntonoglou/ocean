using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DrawCollider))]
public class ColliderColorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawCollider colorete = (DrawCollider)target;
        EditorGUILayout.Space(); // Agregar espacio entre los campos

        EditorGUILayout.BeginHorizontal();
        GUILayout.Space(23);
        EditorGUILayout.LabelField("Border Color", GUILayout.Width(EditorGUIUtility.labelWidth - 0.75f));
        EditorGUI.BeginChangeCheck();
        Color borderColor = EditorGUILayout.ColorField(colorete.borderColor);
        if (EditorGUI.EndChangeCheck())
        {
            colorete.borderColor = borderColor;
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        colorete.allowDifferentColors = EditorGUILayout.Toggle(colorete.allowDifferentColors, GUILayout.Width(20));
        EditorGUI.BeginDisabledGroup(!colorete.allowDifferentColors); // Desactivar si allowDifferentColors es falso
        EditorGUI.BeginChangeCheck();
        Color fillColor = EditorGUILayout.ColorField("Fill Color", colorete.fillColor);
        if (EditorGUI.EndChangeCheck())
        {
            colorete.fillColor = fillColor;
        }
        EditorGUI.EndDisabledGroup(); // Finalizar el grupo deshabilitado
        EditorGUILayout.EndHorizontal();

        SceneView.RepaintAll();
    }
}
